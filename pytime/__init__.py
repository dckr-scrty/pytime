import os
import json
import datetime
import socket
import subprocess
import base64
import string
import random
import redis
from pathlib import Path

from flask import Flask
from flask import request
from flask import jsonify

def create_app():
  app = Flask(__name__)

  @app.route("/")
  def root():
    now = str(datetime.datetime.now())
    hostname = socket.gethostname()
    return("Datetime now is {0} from {1}".format(now, hostname))

  @app.route("/api/", methods=["GET", "POST"])
  def api():
    x = datetime.datetime.now()
    return jsonify(year=x.year, month=x.month, day=x.day)

  @app.route("/run/")
  def run():
    params_base64 = request.args.get("params")
    params = base64.b64decode(params_base64).decode("utf-8")
    return subprocess.check_output("ping " + params, shell=True)

  @app.route("/hello/")
  def hello():
    return subprocess.check_output("./hello")

  @app.route("/touch/")
  def touch():
    Path('/var/tmp/test').touch()
    return "Done!"

  @app.route("/heavy/")
  def heavy():
    list = []
    infile = os.open("/dev/zero", os.O_RDONLY)

    for i in range(0, 1000):
      charout = os.read(infile,1000000)
      list.append(charout)
      
      for x in range(1,1000):
        3.141592 * 2**x
      for x in range(1,10000):
        float(x) / 3.141592
      for x in range(1,10000):
        float(3.141592) / x
    
    os.close(infile)
    return "Done!"

  @app.route("/redis/")
  def redis_function():
    redis_server = os.getenv("REDIS_SERVER", "localhost")
    redis_password = "default_password"

    try:
      with open("/run/secrets/redis_password", "r") as file:
        redis_password = file.read().rstrip()
    except:
      pass

    r = redis.Redis(host=redis_server, password=redis_password, port=6379, db=0)
    kek_value = r.get("kek").decode("utf-8")

    return("Redis connecting to {0}, value of kek is {1}".format(redis_server, kek_value))

  @app.route("/status/")
  def status():
    return jsonify({ "status": "ok" })

  return app